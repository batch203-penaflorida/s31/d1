// Use the "require" directive to load Node js Modules
//  "http module" let's Node.js transfer data using the Hypertext Transfer Protocol.

// This is a set of individual files that contain code to create a "component" that helps establish data transfer between application.
// allows us to fetch resources such as HTML documents.
// A "module" is a software component or part of a program that contains one or more routines.
// Set of individual file to create a component => Module.

// Clients (web browser) and servers (nodeJS/expressJS application) communicate by exchanging individual messages (requests/responses).
// The message sent by the client is called "request"
// The message sent by the server as the answer is called "response".
let http = require("http");

// Using this module's "createServer()" method, we can create an HTTP server that listens to the represents on a specified port and gives response back to the client.

// initiate action - client -> giving request <- server gives the response
// A port is a virtual point where network connections start and end.

// Each port is associated with specific process or service.

// 0 to 1023 - Reserved

// The server will be assigned to port 4000 via the "listen()" method where the server will listen to any request that are sent to it and will also send the response on via this port.

http
  .createServer(function (request, response) {
    //   Use to the writeHead() method:
    //   Set a status code for the response. (200 means "OK")
    //   Set the content-type of the response. (plain text message).
    //  response.writeHead(200, { "Content-Type": "text/plain" });
    //  response.end("Hello World");
    response.writeHead(200, { "Content-Type": "text/html" });
    response.end("<h1>Hello World</h1>");
  })
  .listen(4000);

console.log("Server is running at localhost:4000");

// npx kill-port 4000

// 3000, 4000, 8000, 5000, = Usually used for web development.

// We will access the server in the browser using the localhost:4000

// localhost - in the local machine.

/* 

Informational responses (100–199)
Successful responses (200–299)
Redirection messages (300–399)
Client error responses (400–499)
Server error responses (500–599)

*/
